// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VeeValidate from 'vee-validate'
import fr from 'vee-validate/dist/locale/fr'
import store from './store/index'
import router from './router'
import Notifications from 'vue-notification'
import 'vuetify/dist/vuetify.min.css'
import Vuetify from 'vuetify'
import vuexI18n from 'vuex-i18n'
import '@mdi/font/css/materialdesignicons.css'
import FlagIcon from 'vue-flag-icon'
import translationEn from '@/locales/en.json'
import translationFr from '@/locales/fr.json'
import VueMoment from 'vue-moment'
import VueJWT from 'vuejs-jwt'
import VueApexCharts from 'vue-apexcharts'
import './assets/sass/main.scss'
import * as VueGoogleMaps from 'vue2-google-maps'
import config from '@/config/config'
import VueVideoPlayer from 'vue-video-player'
import 'video.js/dist/video-js.css'

Vue.use(VueVideoPlayer)

Vue.component('apexchart', VueApexCharts)

let vuexStorage = JSON.parse(localStorage.getItem('vuex'))

Vue.use(VueGoogleMaps, {
  load: {
    key: config.googleApiKey
  }
})
Vue.use(vuexI18n.plugin, store)
Vue.i18n.add('fr', translationFr)
Vue.i18n.add('en', translationEn)
if (vuexStorage !== null && vuexStorage.i18n.locale !== undefined) {
  Vue.i18n.set(vuexStorage.i18n.locale)
} else {
  Vue.i18n.set('fr')
}

const configVeeValidate = {
  fieldsBagName: 'fieldsVeeValidate',
  events: '',
  locale: Vue.i18n.locale(),
  dictionary: {
    fr: fr
  }
}

VeeValidate.Validator.extend('card_number', {
  validate (value, args) {
    let returnValue = false
    if (value) {
      let splitedValue = value.split('-')
      if (splitedValue.length === 4) {
        splitedValue.forEach(element => {
          element.length === 4 && Number.isInteger(Number(element)) ? returnValue = true : returnValue = false
        })
      }
    }
    return returnValue
  }
})

Vue.use(VeeValidate, configVeeValidate)
const dict = {
  en: {
    messages: {
      card_number: (field, args) => `Credit card number invalid.`
    }
  },
  fr: {
    messages: {
      card_number: (field, args) => `Numéro de carde de crédit invalide.`
    }
  }
}
VeeValidate.Validator.localize(dict)

Vue.use(Notifications)
Vue.use(FlagIcon)
Vue.use(VueMoment)
Vue.use(VueJWT)

Vue.use(Vuetify, {
  iconfont: 'mdi'
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  vuetify: new Vuetify(),
  components: { App },
  template: '<App/>'
})
