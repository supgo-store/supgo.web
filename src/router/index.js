import Vue from 'vue'
import Router from 'vue-router'
import Moment from 'moment'
import store from '@/store/index'
import config from '@/config/config'

import Home from '@/components/Home'
import Login from '@/components/Login'
import UserDetail from '@/components/UserDetail'
import Comments from '@/components/Comments'
import StockShop from '@/components/StockShop'
import Users from '@/components/Users'
import Promotions from '@/components/Promotions'
import TransactionHistory from '@/components/TransactionHistory'
import TransactionHistoryUser from '@/components/TransactionHistoryUser'
import SaleStats from '@/components/SaleStats'
import Products from '@/components/Products'
import AllProducts from '@/components/AllProducts'
import ProductDetail from '@/components/ProductDetail'

Vue.use(Router)

var router = new Router({
  mode: 'history',
  routes: [{
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/comments',
    name: 'comments',
    component: Comments,
    meta: {
      role: 'supgo-api-admin'
    }
  },
  {
    path: '/userDetail',
    name: 'userDetail',
    component: UserDetail,
    meta: {
      role: 'supgo-api-user'
    }
  },
  {
    path: '/stock',
    name: 'stock',
    component: StockShop,
    meta: {
      role: 'supgo-api-admin'
    }
  },
  {
    path: '/promotions',
    name: 'promotions',
    component: Promotions,
    meta: {
      role: 'supgo-api-admin'
    }
  },
  {
    path: '/users',
    name: 'users',
    component: Users,
    meta: {
      role: 'supgo-api-admin'
    }
  },
  {
    path: '/transactionHistory',
    name: 'transactionHistory',
    component: TransactionHistory,
    meta: {
      role: 'supgo-api-admin'
    }
  },
  {
    path: '/transactionHistoryUser',
    name: 'transactionHistoryUser',
    component: TransactionHistoryUser,
    meta: {
      role: 'supgo-api-user'
    }
  },
  {
    path: '/saleStats',
    name: 'saleStats',
    component: SaleStats,
    meta: {
      role: 'supgo-api-admin'
    }
  },
  {
    path: '/products',
    name: 'products',
    component: Products,
    meta: {
      role: 'supgo-api-admin'
    }
  },
  {
    path: '/productDetail/:productId/',
    name: 'productDetail',
    component: ProductDetail,
    meta: {
      role: 'supgo-api-user'
    }
  },
  {
    path: '/allProducts',
    name: 'allProducts',
    component: AllProducts
  },
  {
    path: '*',
    redirect: '/'
  }
  ]
})

router.beforeEach(async (to, from, next) => {
  if ((!store.state.keycloak || !store.state.keycloak.token || !store.state.keycloak.refreshToken || ((Vue.$jwt.decode(store.state.keycloak.refreshToken).exp - config.diffGMTMin * 60) - Moment().unix()) < 0) && (to.name !== 'login' && to.name !== 'home')) {
    if (((Vue.$jwt.decode(store.state.keycloak.refreshToken).exp - config.diffGMTMin * 60) - Moment().unix()) < 0) {
      store.dispatch('keycloak', null)
      router.push('home')
    }
    next({
      name: 'home'
    })
  } else if (store.state.keycloak && store.state.keycloak.token && store.state.keycloak.refreshToken && ((Vue.$jwt.decode(store.state.keycloak.refreshToken).exp - config.diffGMTMin * 60) - Moment().unix() > 0) && to.name === 'login') {
    next({
      name: 'home'
    })
  } else {
    if (to.meta && to.meta.role) {
      if (!Vue.$jwt.decode(store.state.keycloak.token).user_roles.find(element => element === to.meta.role)) {
        next({
          name: 'home'
        })
      }
    }
    next()
  }
})

export default router
