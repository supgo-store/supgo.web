import { api } from '@/services/Api'

export default {
  index () {
    return api.get(`/api/Product`)
  },
  indexByCategory (uuid) {
    return api.post(`/api/Product/filtered-list`, {
      'Key': 'CategoryUuid',
      'Value': uuid
    })
  },
  indexByUuid (uuid) {
    return api.get(`/api/Product/` + uuid)
    // , {
    //   'Key': 'ProductUuid',
    //   'Value': uuid
    // })
  },
  update (newItem) {
    return api.put(`/api/Product`, newItem)
  },
  create (newItem) {
    return api.post(`/api/Product`, newItem)
  },
  delete (uuid) {
    return api.delete(`/api/Product/${uuid}`)
  }
}
