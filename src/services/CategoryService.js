import { api } from '@/services/Api'

export default {
  index () {
    return api.get(`/api/Category`)
  }
}
