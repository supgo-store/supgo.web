import { unLogged } from '@/services/Api'

export default {
  index () {
    return unLogged.get(`/api/Store`)
  }
}
