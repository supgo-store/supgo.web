import { api } from '@/services/Api'

export default {
  index () {
    return api.get(`/api/Stock`)
  },
  update (stock, quantity) {
    return api.put(`/api/Stock`,
      {
        idStock: stock.idStock,
        productUuid: stock.productUuid,
        quantity: quantity,
        storeUuid: stock.storeUuid,
        uuid: stock.uuid
      }
    )
  }
}
