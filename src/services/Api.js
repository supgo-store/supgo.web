import Axios from 'axios'
import securityService from '@/services/SecurityService'
import store from '@/store/index'
import Vue from 'vue'
import Moment from 'moment'
import router from '@/router/index'
import env from '@/config/config'

export const auth = Axios.create({
  baseURL: env.keycloakURL
})

export const api = Axios.create({
  baseURL: env.apiURL
})

export const unLogged = Axios.create({
  baseURL: env.apiURL
})

async function interceptRequest (config) {
  if (!store.state.keycloak || !store.state.keycloak.token || !store.state.keycloak.refreshToken || ((Vue.$jwt.decode(store.state.keycloak.refreshToken).exp - env.diffGMTMin * 60) - Moment().unix()) < 0) {
    store.dispatch('keycloak', null)
    router.push('home')
  } else if (((Vue.$jwt.decode(store.state.keycloak.token).exp - env.diffGMTMin * 60) - Moment().unix()) < 0) {
    try {
      let response = await securityService.refreshToken(store.state.keycloak.refreshToken)
      store.dispatch('setToken', response.data.access_token)
      store.dispatch('setRefreshToken', response.data.refresh_token)
    } catch (error) {
      console.error(error)
      store.dispatch('keycloak', null)
      var url = `${env.keycloakURL}/auth/realms/${env.realm}/protocol/openid-connect/logout?redirect_uri=${encodeURIComponent(`${env.frontURL}/`)}`
      window.location.href = url
    }
  }
  config.headers = {
    Authorization: store.state.keycloak && store.state.keycloak.token ? `Bearer ${store.state.keycloak.token}` : ''
  }
  return config
}

async function errorResponse (error) {
  if (error.response.status === 401) {
    store.dispatch('keycloak', null)
    var url = `${env.keycloakURL}/auth/realms/${env.realm}/protocol/openid-connect/logout?redirect_uri=${encodeURIComponent(`${env.frontURL}/`)}`
    window.location.href = url
  }
}

api.interceptors.response.use((response) => {
  return response
}, (error) => errorResponse(error)
)

api.interceptors.request.use((config) => interceptRequest(config))
