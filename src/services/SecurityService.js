import { auth } from '@/services/Api'
import qs from 'qs'
import config from '@/config/config'

export default {
  refreshToken (refreshtoken) {
    return auth.post(`/auth/realms/${config.realm}/protocol/openid-connect/token`, qs.stringify({
      grant_type: 'refresh_token',
      refresh_token: refreshtoken,
      client_id: config.clientId
    }),
    {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
  }
}
