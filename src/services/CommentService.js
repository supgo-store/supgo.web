import { api } from '@/services/Api'

export default {
  index () {
    return api.get(`/api/Comment`)
  },
  indexByProduct (uuid) {
    return api.post(`/api/Comment/filtered-list`, {
      'Key': 'ProductUuid',
      'Value': uuid
    })
  },
  create (newComment) {
    return api.post(`/api/comment`, newComment)
  },
  delete (uuid) {
    return api.delete(`/api/Comment/${uuid}`)
  }
}
