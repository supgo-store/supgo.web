import { api } from '@/services/Api'

export default {
  index () {
    return api.get(`/api/Discount`)
  },
  indexByProduct (uuid) {
    return api.post(`/api/Discount/filtered-list`, {
      'Key': 'ProductUuid',
      'Value': uuid
    })
  },
  update (newItem) {
    return api.put(`/api/Discount`, newItem)
  },
  create (newItem) {
    return api.post(`/api/Discount`, newItem)
  },
  delete (uuid) {
    return api.delete(`/api/Discount/${uuid}`)
  }
}
