import { api } from '@/services/Api'

export default {
  index () {
    return api.get(`/api/Orders`)
  },
  indexByCart (uuid) {
    return api.post(`/api/Orders/filtered-list`, {
      'Key': 'CartUuid',
      'Value': uuid
    })
  }
}
