import { api } from '@/services/Api'

export default {
  index () {
    return api.get(`/api/Cart`)
  },
  indexByUser (uuid) {
    return api.post(`/api/Cart/filtered-list`, {
      'Key': 'UserUuid',
      'Value': uuid
    })
  }
}
