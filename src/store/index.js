import Vue from 'vue'
import Vuex from 'vuex'
import PersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    keycloak: null
  },
  mutations: {
    keycloak (state, keycloak) {
      state.keycloak = keycloak
    },
    setToken (state, token) {
      state.keycloak.token = token
      state.keycloak.tokenParsed = Vue.$jwt.decode(token)
    },
    setRefreshToken (state, refreshToken) {
      state.keycloak.refreshToken = refreshToken
      state.keycloak.refreshTokenParsed = Vue.$jwt.decode(refreshToken)
    }
  },
  actions: {
    keycloak (context, keycloak) {
      context.commit('keycloak', keycloak)
    },
    setToken (context, token) {
      context.commit('setToken', token)
    },
    setRefreshToken (context, refreshToken) {
      context.commit('setRefreshToken', refreshToken)
    }
  },
  getters: {
    getKeycloak: state => {
      return state.keycloak
    }
  },
  plugins: [PersistedState()]
})
