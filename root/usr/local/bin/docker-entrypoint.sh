#!/bin/sh

set -e

RED_COLOR="\e[91m"
DEFAULT_COLOR="\033[0m"

####
### Set environment variable in app
##
echo "Setting environment variable in app"

exit_script=false

if [ -z "${KEYCLOAK_URL}" ]
then
    exit_script=true
    echo -e "${RED_COLOR}KEYCLOAK_URL not set.${DEFAULT_COLOR}"
elif [ -z "${API_URL}" ]
then
    exit_script=true
    echo -e "${RED_COLOR}API_URL not set.${DEFAULT_COLOR}"
elif [ -z "${FRONT_URL}" ]
then
    exit_script=true
    echo -e "${RED_COLOR}FRONT_URL not set.${DEFAULT_COLOR}"
elif [ -z "${REALM}" ]
then
    exit_script=true
    echo -e "${RED_COLOR}REALM not set.${DEFAULT_COLOR}"
elif [ -z "${CLIENT_ID}" ]
then
    exit_script=true
    echo -e "${RED_COLOR}CLIENT_ID not set.${DEFAULT_COLOR}"
elif [ -z "${MAIL_SUPPORT}" ]
then
    exit_script=true
    echo -e "${RED_COLOR}MAIL_SUPPORT not set.${DEFAULT_COLOR}"
elif [ -z "${GOOGLE_API_KEY}" ]
then
    exit_script=true
    echo -e "${RED_COLOR}GOOGLE_API_KEY not set.${DEFAULT_COLOR}"
elif [ -z "${DIFF_GMT_MIN}" ]
then
    exit_script=true
    echo -e "${RED_COLOR}DIFF_GMT_MIN not set.${DEFAULT_COLOR}"
fi

FILE=$(find static/js -type f -name "app*.js")

if [ ! -e $FILE ]
then
    exit_script=true
    echo -e "${RED_COLOR}App file not found${DEFAULT_COLOR}"
fi

if [ "$exit_script" = true ]
then
     exit 1
fi

echo "Set environment variable in app"

sed -i \
    -e "s#%KEYCLOAK_URL%#${KEYCLOAK_URL}#" \
    -e "s#%FRONT_URL%#${FRONT_URL}#" \
    -e "s#%API_URL%#${API_URL}#" \
    -e "s#%MAIL_SUPPORT%#${MAIL_SUPPORT}#" \
    -e "s#%REALM%#${REALM}#" \
    -e "s#%CLIENT_ID%#${CLIENT_ID}#" \
    -e "s#%GOOGLE_API_KEY%#${GOOGLE_API_KEY}#" \
    -e "s#%DIFF_GMT_MIN%#${DIFF_GMT_MIN}#" \
    ${FILE}

echo "App is ready"

exec "$@"
