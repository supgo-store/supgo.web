# étape de build
FROM node:lts-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm ci
COPY . .
RUN cp src/config/config.js.dist src/config/config.js
RUN npm run build

# étape de production
FROM nginx:stable-alpine as production-stage
WORKDIR /usr/share/nginx/html

ADD root /

COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80

RUN chmod +x /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]

CMD ["nginx", "-g", "daemon off;"]